<?php

use Illuminate\Database\Seeder;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drinks')->insert([
        	'name'=> 'MonsterEnergy',
        	'comments'=>'Release the Beast',
        	'rating'=>3,
        	'brew_date'=>'2015-09-20',
        	]);
    }
}
